package com.arabia.jerseycrud.service;

import javax.ws.rs.core.Response;

import com.erabia.student_hibernate.bean.Student;

public interface StudentWebService {

	public Response addStudent(Student student);

	public Response updateStudent(Student student);

	public Response getAllStudents();
	public Response getStudentById(int id);

	public Response deleteStudent(int id);

}
