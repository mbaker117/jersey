package com.arabia.jerseycrud.service.impl;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.arabia.jerseycrud.service.StudentWebService;
import com.erabia.student_hibernate.bean.Student;
import com.erabia.student_hibernate.dao.StudentHibernateDAO;
import com.erabia.student_hibernate.dao.impl.StudentHibernateDAOImpl;
import com.erabia.student_hibernate.exception.StudentHibernateException;

@Path("/StudentWebServiceImpl")
public class StudentWebServiceImpl implements StudentWebService {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addStudent(Student student) {

		if (student == null || student.getFirstName() == null || student.getLastName() == null)
			return Response.status(400).build();

		try {

			StudentHibernateDAO studentHibernateDAO = new StudentHibernateDAOImpl();

			studentHibernateDAO.add(student);

			return Response.status(201).entity(student).build();
		} catch (StudentHibernateException e) { // TODO Auto-generated catch block
			return Response.status(500).build();
		}

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public Response updateStudent(Student student) { // TODO Auto-generated method
		if (student == null || student.getFirstName() == null || student.getLastName() == null)
			return Response.status(400).build();

		try {

			StudentHibernateDAO studentHibernateDAO = new StudentHibernateDAOImpl();
			if (!studentHibernateDAO.get(student.getId()).isPresent())
				return Response.status(404).build();

			studentHibernateDAO.update(student);

			return Response.status(201).entity(student).build();
		} catch (StudentHibernateException e) { // TODO Auto-generated catch block return
			return Response.status(500).build();
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public Response getAllStudents() {
		// TODO Auto-generated method stub
		try {

			StudentHibernateDAO studentHibernateDAO = new StudentHibernateDAOImpl();
			Optional<List<Student>> optional = studentHibernateDAO.getAll();

			
			
			if (optional.isPresent()) {
				
				return Response.status(200).entity(optional.get()).build();
			} else {
				return Response.status(404).build();
			}

		} catch (StudentHibernateException e) { // TODO Auto-generated catch block
			return Response.status(500).build();
		}
	}

	@DELETE
	@Path("/{id}")
	@Override
	public Response deleteStudent(@PathParam("id") int id) {
		// TODO Auto-generated method stub
		try {

			StudentHibernateDAO studentHibernateDAO = new StudentHibernateDAOImpl();
			if (!studentHibernateDAO.get(id).isPresent())
				return Response.status(404).build();

			studentHibernateDAO.delete(id);

			return Response.status(204).build();
		} catch (StudentHibernateException e) { // TODO Auto-generated catch block
			return Response.status(500).build();
		}

	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@Override
	public Response getStudentById(@PathParam("id")int id) {
		// TODO Auto-generated method stub
		try {

			StudentHibernateDAO studentHibernateDAO = new StudentHibernateDAOImpl();
			Optional<Student> optional = studentHibernateDAO.get(id);
			if (!optional.isPresent())
				return Response.status(404).build();

			return Response.status(200).entity(optional.get()).build();
		} catch (StudentHibernateException e) { // TODO Auto-generated catch block return
			return Response.status(500).build();
		}

	}
}
