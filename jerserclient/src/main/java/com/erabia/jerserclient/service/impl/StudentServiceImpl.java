package com.erabia.jerserclient.service.impl;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;

import com.erabia.jerserclient.bean.Student;
import com.erabia.jerserclient.exception.StudentException;
import com.erabia.jerserclient.exception.enums.StudentExceptionType;
import com.erabia.jerserclient.service.StudentService;

public class StudentServiceImpl implements StudentService {
	private static String URL = "http://localhost:8888/jerseycrud/student/StudentWebServiceImpl";

	@Override
	public Student add(Student student) throws StudentException {
		// TODO Auto-generated method stub
		if (student == null)
			throw new IllegalArgumentException("student cannot be null");
		if (student.getAvg() < 0 || student.getAvg() > 100)
			throw new StudentException(StudentExceptionType.INVALID_AVG, StudentExceptionType.INVALID_AVG.getMsg());
		Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
		WebTarget webTarget = client.target(URL).path("");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(student, MediaType.APPLICATION_JSON));
		if (response.getStatus() == 400)
			throw new StudentException(StudentExceptionType.BAD_REQUEST, StudentExceptionType.BAD_REQUEST.getMsg());
		return response.readEntity(Student.class);
	}

	@Override
	public Student update(Student student) throws StudentException {
		if (student == null)
			throw new IllegalArgumentException("student cannot be null");
		if (student.getAvg() < 0 || student.getAvg() > 100)
			throw new StudentException(StudentExceptionType.INVALID_AVG, StudentExceptionType.INVALID_AVG.getMsg());
		Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
		WebTarget webTarget = client.target(URL).path("");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.put(Entity.entity(student, MediaType.APPLICATION_JSON));
		if (response.getStatus() == 400)
			throw new StudentException(StudentExceptionType.BAD_REQUEST, StudentExceptionType.BAD_REQUEST.getMsg());
		if (response.getStatus() == 404)
			throw new StudentException(StudentExceptionType.NO_EXISTING_STUDENT,
					StudentExceptionType.NO_EXISTING_STUDENT.getMsg());

		return response.readEntity(Student.class);
	}

	@Override
	public List<Student> getAllStudents() {
		// TODO Auto-generated method stub
		Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
		WebTarget webTarget = client.target(URL).path("");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		return response.readEntity(List.class);
	}

	@Override
	public Student getStudentById(int id) throws StudentException {
		// TODO Auto-generated method stub
		Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
		WebTarget webTarget = client.target(URL).path("" + id);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		if (response.getStatus() == 404) {
			throw new StudentException(StudentExceptionType.NO_EXISTING_STUDENT,
					StudentExceptionType.NO_EXISTING_STUDENT.getMsg());
		}
		return response.readEntity(Student.class);

	}

	@Override
	public void delete(int id) throws StudentException {
		// TODO Auto-generated method stub
		Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
		WebTarget webTarget = client.target(URL).path("" + id);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.delete();
		if (response.getStatus() == 404) {
			throw new StudentException(StudentExceptionType.NO_EXISTING_STUDENT,
					StudentExceptionType.NO_EXISTING_STUDENT.getMsg());
		}
	}

}
