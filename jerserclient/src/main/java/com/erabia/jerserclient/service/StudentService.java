package com.erabia.jerserclient.service;

import java.util.List;

import com.erabia.jerserclient.bean.Student;
import com.erabia.jerserclient.exception.StudentException;

public interface StudentService {
	
	public Student add(Student student) throws StudentException;
	public Student update(Student student) throws StudentException;
	public List<Student> getAllStudents();
	public Student getStudentById(int id) throws StudentException;
	public void delete(int id) throws StudentException;

}
