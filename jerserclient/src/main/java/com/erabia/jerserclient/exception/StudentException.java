package com.erabia.jerserclient.exception;

import com.erabia.jerserclient.exception.enums.StudentExceptionType;

public class StudentException  extends Exception{
	private final StudentExceptionType type;
	
	public StudentException(final StudentExceptionType type, final String message) {
		super(message);
		this.type = type;
	
	}
	public StudentExceptionType getType() {
		return type;
	}

	

}
