package com.erabia.jerserclient.exception.enums;

public enum StudentExceptionType {

	MISSING_CONNECTION("connection can't be established."), EXISTING_STUDENT("student already exists"),
	NO_EXISTING_STUDENT("student not found"), INVALID_AVG("student avg is invalid"),
	CLASS_NOT_FOUND("class not found in connection"), SQL_EXCEPTION("error in sql"), IO_EXCEPTION("error in io"),BAD_REQUEST("bad request");

	private String msg;

	private StudentExceptionType(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}
}
